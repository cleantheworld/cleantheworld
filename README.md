# CleanTheWorld

## Instrukcje instalacji
[backend](https://gitlab.com/cleantheworld/cleantheworld/-/tree/main/services/backend)
[frontend](https://gitlab.com/cleantheworld/cleantheworld/-/tree/main/services/frontend)

## Nazwa projektu
CleanTheWorld / "Sprzątanie świata"

## Nazwa drużyny
CleanTheWorld Team

## Skład drużyny
[Stanisław Nieradko](https://nieradko.com) - koordynator, programista backendu

[Błażej Sak](https://github.com/bsak2003) - programista backendu

[Piotr Trybisz](https://ptrybisz.tk) - programista frontendu

## Link do pracy
[https://cleantheworld.bazik.xyz/](https://cleantheworld.bazik.xyz/) - strona internetowa

[https://cleantheworld.bazik.xyz/api/swagger](https://cleantheworld.bazik.xyz/api/swagger) - API

## Link do kodu źródłowego
[https://gitlab.com/cleantheworld/cleantheworld](https://gitlab.com/cleantheworld/cleantheworld)

## Na jakie potrzeby/problemy odpowiada Wasze rozwiązanie?
Na świecie jest bardzo dużo śmieci w miejscach do tego nie przeznaczonych. W 1989 w Australii zostało zorganizowane pierwsze Sprzątanie Świata (ang. Clean Up the World), które przeobraziło się w coroczną kampanię odbywającą się w trzeci weekend września koordynowaną przez organizację społeczną Clean up Australia. Do Polski akcja została sprowadzona w 1993 roku przez Fundację Nasza Ziemia prowadzoną przez Mirę Stanisławską-Meysztowicz. 

Nasza aplikacja służy do wspomagania takich akcji. Za jej pomocą można oznaczyć zalegające śmieci na mapie. Następnie, gdy śmieci zostaną wyrzucone, można je oznaczyć w aplikacji jako zebrane - za co osoba otrzymuje punkty. 

## W jakich językach programowania, jakich technologiach powstała aplikacja/narzędzie?
C#, .NET, ASP.NET Core - backend

Svelete, TypeScript - frontend

Leaflet - obsługa mapy

## Opisz działanie Waszej aplikacji/narzędzia
Aplikacja obecnie pozwala na zgłoszenie pozostawionych śmieci na mapie. Śmieci takie pojawiają się na mapie. Następnie dowolna osoba (zarówno ta, która zgłosiła odpady jak i zupełnie inna) zbiera śmieci i oznacza je jako zebrane. Za każde takie działanie użytkownik otrzymuje punkty. Można również "zarezerwować" odpad, tzn. oznaczyć go na mapie, tak by inni użytkownicy wiedzieli, że ktoś zamierza posprzątać dany teren. 

## Jak widzicie dalszy rozwój Waszego rozwiązania?
- sklep z nagrodami za punkty dla użytkowników programu
- lepszy podział mapy (oznaczenie miejsc, w których ryzyko pojawienia się niepożądanych odpadów jest większe, takich jak np. lasy)
- funkcja zwiadu (tj. zlecenie sprawdzenia danego obszaru, na którym wcześniej wystąpiły odpady, chętnemu użytkownikowi)
- statystyki i API dla programistów (co umożliwiłoby integrację np. z systemami lokalnych władz w celu powstrzymania nielegalnych wywozów śmieci)
- wydarzenia (możliwość umówienia się przez aplikację na Sprzątanie Świata w większej grupie)
- refactoring i optymalizacja aplikacji (z racji ograniczonego czasu, wiele rzeczy nie jest zrobione w sposób optymalny lub czytelny)
- oznaczenie punktów, w których można bezpiecznie wyrzucić dane odpady (dla osób, które zebrały odpady i nie wiedzą co z nimi zrobić lub dla osób chcących po prostu wyrzucić odpad danego typu)
- aplikacja mobilna (ułatwienie dostępu do aplikacji)

## Jakie widzicie zagrożenia/ryzyka dla Waszego rozwiązania?
- konieczność posiadnaia odpowiedniego sprzętu (zarówno po stronie naszej - serwerów, jak i potencjalnych użytkowników) i oprgoramowania (dostawcy map)
- konieczność znalezienia sponsorów nagród w sklepie (np. organizacji charytatywnych lub samorządów)
- promocja aplikacji wśród potencjalnych użytkowników
- moderacja danych wprowadzanych przez użytkowników
- weryfikacja, czy śmieci faktycznie zostały sprzątnięte (np. za pomocą zdjęć)

## Dlaczego akurat to Wy powinniście wygrać?
- unikatowy pomysł
- aplikacja służy ważnemu celowi społecznemu - ochronie środowiska!
- stworzyliśmy działający prototyp produktu
- duże możliwości rozwoju
- nowoczesna architektura (zarówno ze strony backendu - ASP.NET Core 5, Domain Driven Design, Clean Architecture, gotowa na testy, frontendu - Svelte, TypeScript, Material Design jak i deploymentu - NUKE, Docker, Docker Compose)
- brak poważnych zagrożeń (tj. wszystkie wymienione są do rozwiązania)

## Osoba do kontaktu
Stanisław Nieradko

## Adres e-mail 
stanislaw@nieradko.com
