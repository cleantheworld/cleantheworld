﻿using System;

namespace CleanTheWorld.Core
{
    public abstract class Entity
    {
        protected Entity()
        {
            //For EF
            
            Id = Guid.NewGuid();
            Created = DateTime.UtcNow;
            Updated = Created;
        }

        protected Entity(Guid id, DateTime created, DateTime updated)
        {
            // For Unit Tests / constructing objects without Reflection
            
            Id = id;
            Created = created;
            Updated = updated;
        }
        
        public Guid Id { get; }
        public DateTime Created { get; }
        public DateTime Updated { get; private set; }

        public void Update(DateTime updated)
        {
            Updated = updated;
        }
    }
}