﻿using System;

namespace CleanTheWorld.Core
{
    public class DomainException : Exception
    {
        public DomainException(string message) : base(message)
        {
            
        }
    }
}