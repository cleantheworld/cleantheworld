﻿namespace CleanTheWorld.Domain.Enums
{
    public enum TrashStatus
    {
        Pending, 
        Taken, 
        Cleaned
    }
}