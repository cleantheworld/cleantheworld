﻿using System;
using CleanTheWorld.Core;

namespace CleanTheWorld.Domain.Entities
{
    public class Spot : Entity
    {
        private Spot()
        {
            //For EF
        }
        
        protected Spot(Guid id, DateTime created, DateTime updated, Coordinates coordinates, string description, string photoUrl, DateTime nextCheckup) 
            : base(id, created, updated)
        {
            Coordinates = coordinates;
            Description = description;
            PhotoUrl = photoUrl;
            NextCheckup = nextCheckup;
        }

        public Spot(Coordinates coordinates, string description, string photoUrl, DateTime nextCheckup)
        {
            Coordinates = coordinates;
            Description = description;
            PhotoUrl = photoUrl;
            NextCheckup = nextCheckup;
        }


        
        public Coordinates Coordinates { get; }
        public string Description { get; private set; }
        public string PhotoUrl { get; private set; }
        public DateTime NextCheckup { get; private set; }
        
        public void UpdateDescription(string description)
        {
            Description = description;
        }

        public void UpdatePhotoUrl(string photoUrl)
        {
            PhotoUrl = photoUrl;
        }

        public void SetNextCheckup(DateTime nextCheckup)
        {
            NextCheckup = nextCheckup;
        }
    }
}