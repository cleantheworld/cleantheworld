﻿using System;
using CleanTheWorld.Core;

namespace CleanTheWorld.Domain.Entities
{
    public class Participant : Entity
    {
        private Participant()
        {
            //For EF
        }
        
        protected Participant(Guid id, DateTime created, DateTime updated, string displayName, string auth0Id) 
            : base(id, created, updated)
        {
            DisplayName = displayName;
            Auth0Id = auth0Id;
            Points = 0;
        }

        public Participant(string displayName, string auth0Id)
        {
            DisplayName = displayName;
            Auth0Id = auth0Id;
            Points = 0;
        }

        public string DisplayName { get; private set; }
        public int Points { get; private set; }
        public string Auth0Id { get; private set; }
        
        public void UpdateDisplayName(string displayName)
        {
            DisplayName = displayName;
        }
        
        public void AddPoints(int points)
        {
            Points += points;
        }
    }
}