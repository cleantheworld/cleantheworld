﻿#nullable enable
using System.Collections.Generic;
using CleanTheWorld.Core;

namespace CleanTheWorld.Domain.Entities
{
    public class Coordinates : ValueObject
    {
        public Coordinates(double longitude, double latitude, double altitude)
        {
            Longitude = longitude;
            Latitude = latitude;
            Altitude = altitude;
        }

        public double Longitude { get; }
        public double Latitude { get; }
        public double Altitude { get; }
        
        protected override IEnumerable<object?> GetEqualityComponents()
        {
            yield return Longitude;
            yield return Latitude;
            yield return Altitude;
        }
    }
}