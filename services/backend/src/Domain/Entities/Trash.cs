﻿using System;
using CleanTheWorld.Core;
using CleanTheWorld.Domain.Enums;

namespace CleanTheWorld.Domain.Entities
{
    public class Trash : Entity
    {
        private Trash()
        {
            //For EF
        }

        protected Trash(Guid id, DateTime created, DateTime updated, Coordinates coordinates, string description, string photoUrl)
            : base(id, created, updated)
        {
            Coordinates = coordinates;
            Description = description;
            PhotoUrl = photoUrl;
        }
        
        public Trash(Coordinates coordinates, string description, string photoUrl)
        {
            Coordinates = coordinates;
            Description = description;
            PhotoUrl = photoUrl;
        }
        
        public Coordinates Coordinates { get; }
        public string Description { get; private set; }
        public string PhotoUrl { get; private set; }

        public TrashStatus Status { get; private set; }
        public Guid AssociatedParticipant { get; private set; }

        public void UpdateDescription(string description)
        {
            Description = description;
        }

        public void UpdatePhotoUrl(string photoUrl)
        {
            PhotoUrl = photoUrl;
        }

        public void SetStatus(TrashStatus status)
        {
            Status = status;
        }

        public void SetAssociatedParticipant(Participant participant)
        {
            AssociatedParticipant = participant.Id;
        }
    }
}