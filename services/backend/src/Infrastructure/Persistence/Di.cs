﻿using System.Reflection;
using CleanTheWorld.Application.Common.Interfaces.Persistence;
using CleanTheWorld.Infrastructure.Persistence.Implementations;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace CleanTheWorld.Infrastructure.Persistence
{
    internal static class Di
    {
        internal static IServiceCollection AddPersistence(this IServiceCollection services)
        {
            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseSqlite("Data Source=cleantheworld.db", x =>
                {
                    x.MigrationsAssembly(typeof(AppDbContext).Assembly.GetName().Name);
                });
            });
            
            services.AddTransient<IParticipantRepository, ParticipantRepository>();
            services.AddTransient<ISpotRepository, SpotRepository>();
            services.AddTransient<ITrashRepository, TrashRepository>();
            
            return services;
        }

        internal static IApplicationBuilder UsePersistence(this IApplicationBuilder app)
        {
            app
                .ApplicationServices.CreateScope().ServiceProvider
                .GetService<AppDbContext>()?.Database.Migrate();

            return app;
        }
    }
}