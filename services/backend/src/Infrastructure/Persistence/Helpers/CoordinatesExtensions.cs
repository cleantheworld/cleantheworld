﻿using System;
using CleanTheWorld.Domain.Entities;

namespace CleanTheWorld.Infrastructure.Persistence.Helpers
{
    internal static class CoordinatesExtensions
    {
        internal static Coordinates CoordinatesFromString(string str)
        {
            var values = str.Split(';');
            return new Coordinates(
                Convert.ToDouble(values[0]),
                Convert.ToDouble(values[1]),
                Convert.ToDouble(values[2]));
        }

        internal static string StringFromCoordinates(Coordinates cord)
        {
            return $"{cord.Longitude};{cord.Latitude};{cord.Altitude}";
        }
    }
}