﻿using CleanTheWorld.Domain.Entities;
using CleanTheWorld.Infrastructure.Persistence.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CleanTheWorld.Infrastructure.Persistence.TypeConfigurations
{
    public class SpotTypeConfiguration : IEntityTypeConfiguration<Spot>
    {
        public void Configure(EntityTypeBuilder<Spot> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Created)
                .HasColumnType("datetime");
            builder.Property(x => x.Updated)
                .HasColumnType("datetime");
            builder.Property(x => x.Coordinates)
                .HasConversion(
                    x=>CoordinatesExtensions.StringFromCoordinates(x),
                    x=>CoordinatesExtensions.CoordinatesFromString(x));
        }
    }
}