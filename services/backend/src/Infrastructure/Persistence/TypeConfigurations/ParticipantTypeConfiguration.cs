﻿using CleanTheWorld.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CleanTheWorld.Infrastructure.Persistence.TypeConfigurations
{
    public class ParticipantTypeConfiguration : IEntityTypeConfiguration<Participant>
    {
        public void Configure(EntityTypeBuilder<Participant> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Created)
                .HasColumnType("datetime");
            builder.Property(x => x.Updated)
                .HasColumnType("datetime");
        }
    }
}