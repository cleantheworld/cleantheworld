﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using CleanTheWorld.Core;
using CleanTheWorld.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace CleanTheWorld.Infrastructure.Persistence
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options) : base(options)
        {
            
        }
        
        public DbSet<Participant> Participants { get; set; }
        public DbSet<Spot> Spots { get; set; }
        public DbSet<Trash> Trashes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            
            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            UpdateLastUpdatedField();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            UpdateLastUpdatedField();
            return base.SaveChangesAsync(cancellationToken);
        }

        private void UpdateLastUpdatedField()
        {
            var entries = ChangeTracker
                .Entries()
                .Where(e => e.Entity is Entity && e.State is EntityState.Modified);

            foreach (var entry in entries)
            {
                var entity = (Entity)entry.Entity;
                entity.Update(DateTime.UtcNow);
                Entry(entity).Property(x => x.Updated).IsModified = true;
            }
        }
    }
}