﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CleanTheWorld.Application.Common.Interfaces.Persistence;
using CleanTheWorld.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace CleanTheWorld.Infrastructure.Persistence.Implementations
{
    public class ParticipantRepository : IParticipantRepository
    {
        private readonly AppDbContext _context;

        public ParticipantRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<Participant> ReadById(Guid id)
            => await _context.Participants.FirstOrDefaultAsync(x => x.Id == id);

        public async Task<IEnumerable<Participant>> ReadAll()
            => await _context.Participants.ToArrayAsync();

        public async Task Create(Participant entity)
        {
            await _context.Participants.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task Update(Participant entity)
        {
            _context.Participants.Update(entity);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Participant entity)
        {
            _context.Participants.Remove(entity);
            await _context.SaveChangesAsync();
        }
    }
}