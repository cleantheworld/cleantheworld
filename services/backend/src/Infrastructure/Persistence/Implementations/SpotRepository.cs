﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CleanTheWorld.Application.Common.Interfaces.Persistence;
using CleanTheWorld.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace CleanTheWorld.Infrastructure.Persistence.Implementations
{
    public class SpotRepository : ISpotRepository
    {
        private readonly AppDbContext _context;

        public SpotRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<Spot> ReadById(Guid id)
            => await _context.Spots.FirstOrDefaultAsync(x => x.Id == id);

        public async Task<IEnumerable<Spot>> ReadAll()
            => await _context.Spots.ToArrayAsync();

        public async Task Create(Spot entity)
        {
            await _context.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task Update(Spot entity)
        {
            _context.Update(entity);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Spot entity)
        {
            _context.Remove(entity);
            await _context.SaveChangesAsync();
        }
    }
}