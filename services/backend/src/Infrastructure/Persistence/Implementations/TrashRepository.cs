﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CleanTheWorld.Application.Common.Interfaces.Persistence;
using CleanTheWorld.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace CleanTheWorld.Infrastructure.Persistence.Implementations
{
    public class TrashRepository : ITrashRepository
    {
        private readonly AppDbContext _context;

        public TrashRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<Trash> ReadById(Guid id)
            => await _context.Trashes.FirstOrDefaultAsync(x => x.Id == id);

        public async Task<IEnumerable<Trash>> ReadAll()
            => await _context.Trashes.ToArrayAsync();

        public async Task Create(Trash entity)
        {
            await _context.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task Update(Trash entity)
        {
            _context.Update(entity);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Trash entity)
        {
            _context.Remove(entity);
            await _context.SaveChangesAsync();
        }
    }
}