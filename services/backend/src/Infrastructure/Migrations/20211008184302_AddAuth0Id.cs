﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CleanTheWorld.Infrastructure.Migrations
{
    public partial class AddAuth0Id : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Auth0Id",
                table: "Participants",
                type: "TEXT",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Auth0Id",
                table: "Participants");
        }
    }
}
