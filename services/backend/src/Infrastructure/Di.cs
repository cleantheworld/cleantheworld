﻿using CleanTheWorld.Infrastructure.Persistence;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace CleanTheWorld.Infrastructure
{
    public static class Di
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
            => services
                .AddPersistence();

        public static IApplicationBuilder UseInfrastructure(this IApplicationBuilder app)
            => app
                .UsePersistence();
    }
}