﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CleanTheWorld.Api.Models;
using CleanTheWorld.Application.Trashes.Command.CollectTrash;
using CleanTheWorld.Application.Trashes.Command.CreateTrash;
using CleanTheWorld.Application.Trashes.Command.DeleteTrash;
using CleanTheWorld.Application.Trashes.Command.ReserveTrash;
using CleanTheWorld.Application.Trashes.Command.UpdateTrash;
using CleanTheWorld.Application.Trashes.Queries.GetAllTrashes;
using CleanTheWorld.Application.Trashes.Queries.GetTrashById;
using CleanTheWorld.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CleanTheWorld.Api.Controllers
{
    [ApiController]
    [Route("trash")]
    public class TrashController : ControllerBase
    {
        private readonly IMediator _mediator;

        public TrashController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IEnumerable<Trash>> GetAllTrashes()
        {
            return await _mediator.Send(new GetAllTrashes());
        }

        [HttpGet("{id:guid}")]
        public async Task<Trash> GetTrashById(Guid id)
        {
            return await _mediator.Send(new GetTrashById(id));
        }

        [Authorize]
        [HttpPost]
        public async Task<Trash> CreateTrash(CreateTrash model)
        {
            return await _mediator.Send(model);
        }

        [Authorize]
        [HttpPut("{id:guid}")]
        public async Task<Trash> UpdateTrash(Guid id, UpdateTrashModel model)
        {
            return await _mediator.Send(new UpdateTrash(id, model.Description, model.PhotoUrl));
        }

        [Authorize]
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteTrash(Guid id)
        {
            await _mediator.Send(new DeleteTrash(id));
            return NoContent();
        }

        [Authorize]
        [HttpPost("{id:guid}/reserve")]
        public async Task<IActionResult> ReserveTrash(Guid id)
        {
            await _mediator.Send(new ReserveTrash(id));
            return Ok();
        }
        
        [Authorize]
        [HttpPost("{id:guid}/collect")]
        public async Task<IActionResult> CollectTrash(Guid id)
        {
            await _mediator.Send(new CollectTrash(id));
            return Ok();
        }
    }
}