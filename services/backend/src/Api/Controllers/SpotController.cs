﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CleanTheWorld.Api.Models;
using CleanTheWorld.Application.Spots.Commands.CreateSpot;
using CleanTheWorld.Application.Spots.Commands.DeleteSpot;
using CleanTheWorld.Application.Spots.Commands.UpdateSpot;
using CleanTheWorld.Application.Spots.Queries.GetAllSpots;
using CleanTheWorld.Application.Spots.Queries.GetSpotById;
using CleanTheWorld.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CleanTheWorld.Api.Controllers
{
    [ApiController]
    [Route("spot")]
    public class SpotController : ControllerBase
    {
        private readonly IMediator _mediator;
        
        public SpotController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IEnumerable<Spot>> GetAllSpots()
        {
            return await _mediator.Send(new GetAllSpots());
        }
        
        [HttpGet("{id:guid}")]
        public async Task<Spot> GetSpotById(Guid id)
        {
            return await _mediator.Send(new GetSpotById(id));
        }

        [Authorize]
        [HttpPost]
        public async Task<Spot> CreateSpot(CreateSpot model)
        {
            return await _mediator.Send(model);
        }

        [Authorize]
        [HttpPut("{id:guid}")]
        public async Task<Spot> UpdateTrash(Guid id, [FromBody]UpdateSpotModel model)
        {
            return await _mediator.Send(new UpdateSpot(id, model.Description, model.PhotoUrl, model.NextCheckup));
        }

        [Authorize]
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteSpot(Guid id)
        {
            await _mediator.Send(new DeleteSpot(id));
            return NoContent();
        }
    }
}