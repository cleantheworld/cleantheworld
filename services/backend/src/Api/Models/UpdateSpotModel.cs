﻿using System;

namespace CleanTheWorld.Api.Models
{
    public class UpdateSpotModel
    {
        public string Description { get; set; }
        public string PhotoUrl { get; set; }
        public DateTime NextCheckup { get; set; }
    }
}