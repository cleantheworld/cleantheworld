﻿namespace CleanTheWorld.Api.Models
{
    public class UpdateTrashModel
    {
        public string Description { get; set;  }
        public string PhotoUrl { get; set;  }
    }
}