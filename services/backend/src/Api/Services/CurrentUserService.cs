﻿using System;
using System.Security.Claims;
using CleanTheWorld.Application.Common.Interfaces;
using Microsoft.AspNetCore.Http;

namespace CleanTheWorld.Api.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        private readonly IHttpContextAccessor _accessor;

        public CurrentUserService(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        public string GetUserId()
        {
            return _accessor.HttpContext?.User.FindFirstValue(ClaimTypes.NameIdentifier);
        }
    }
}