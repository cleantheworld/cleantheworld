﻿using System.Threading;
using System.Threading.Tasks;
using CleanTheWorld.Application.Common.Interfaces.Persistence;
using CleanTheWorld.Domain.Entities;
using MediatR;

namespace CleanTheWorld.Application.Spots.Queries.GetSpotById
{
    public class GetSpotByIdHandler : IRequestHandler<GetSpotById, Spot>
    {
        private readonly ISpotRepository _repository;

        public GetSpotByIdHandler(ISpotRepository repository)
        {
            _repository = repository;
        }

        public async Task<Spot> Handle(GetSpotById request, CancellationToken cancellationToken)
        {
            return await _repository.ReadById(request.Id);
        }
    }
}