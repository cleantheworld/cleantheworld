﻿using System;
using CleanTheWorld.Domain.Entities;
using MediatR;

namespace CleanTheWorld.Application.Spots.Queries.GetSpotById
{
    public class GetSpotById : IRequest<Spot>
    {
        public GetSpotById(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}