﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CleanTheWorld.Application.Common.Interfaces.Persistence;
using CleanTheWorld.Domain.Entities;
using FluentValidation;

namespace CleanTheWorld.Application.Spots.Queries.GetSpotById
{
    public class GetSpotByIdValidator : AbstractValidator<GetSpotById>
    {
        private readonly ISpotRepository _repository;
        
        public GetSpotByIdValidator(ISpotRepository repository)
        {
            _repository = repository;

            RuleFor(x => x.Id)
                .NotEmpty()
                .MustAsync(Exist).WithMessage("Not Found");
        }
        
        private async Task<bool> Exist(Guid id, CancellationToken cancellationToken) 
            => await _repository.ReadById(id) is not null;
    }
}