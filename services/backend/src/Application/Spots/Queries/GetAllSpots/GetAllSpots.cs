﻿using System.Collections.Generic;
using CleanTheWorld.Domain.Entities;
using MediatR;

namespace CleanTheWorld.Application.Spots.Queries.GetAllSpots
{
    public class GetAllSpots : IRequest<IEnumerable<Spot>>
    {
        
    }
}