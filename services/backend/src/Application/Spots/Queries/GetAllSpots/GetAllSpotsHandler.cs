﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CleanTheWorld.Application.Common.Interfaces.Persistence;
using CleanTheWorld.Domain.Entities;
using MediatR;

namespace CleanTheWorld.Application.Spots.Queries.GetAllSpots
{
    public class GetAllSpotsHandler : IRequestHandler<GetAllSpots, IEnumerable<Spot>>
    {
        private readonly ISpotRepository _repository;

        public GetAllSpotsHandler(ISpotRepository repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<Spot>> Handle(GetAllSpots request, CancellationToken cancellationToken)
        {
            return await _repository.ReadAll();
        }
    }
}