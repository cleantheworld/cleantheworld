﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CleanTheWorld.Application.Common.Interfaces.Persistence;
using CleanTheWorld.Domain.Entities;
using MediatR;

namespace CleanTheWorld.Application.Spots.Commands.CreateSpot
{
    public class CreateSpotHandler : IRequestHandler<CreateSpot, Spot>
    {
        private readonly ISpotRepository _repository;

        public CreateSpotHandler(ISpotRepository repository)
        {
            _repository = repository;
        }

        public async Task<Spot> Handle(CreateSpot request, CancellationToken cancellationToken)
        {
            var spot = new Spot(request.Coordinates, request.Description, request.PhotoUrl, request.NextCheckup);
            
            await _repository.Create(spot);
            
            return spot;
        }
    }
}