﻿using System;
using CleanTheWorld.Domain.Entities;
using MediatR;

namespace CleanTheWorld.Application.Spots.Commands.CreateSpot
{
    public class CreateSpot : IRequest<Spot>
    {
        public CreateSpot(Coordinates coordinates, string description, string photoUrl, DateTime nextCheckup)
        {
            Coordinates = coordinates;
            Description = description;
            PhotoUrl = photoUrl;
            NextCheckup = nextCheckup;
        }

        public Coordinates Coordinates { get; set; }
        public string Description { get; set;  }
        public string PhotoUrl { get; set; }
        public DateTime NextCheckup { get; set; }
    }
}