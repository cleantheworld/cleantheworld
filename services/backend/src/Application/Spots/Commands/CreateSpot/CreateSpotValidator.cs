﻿using System;
using FluentValidation;

namespace CleanTheWorld.Application.Spots.Commands.CreateSpot
{
    public class CreateSpotValidator : AbstractValidator<CreateSpot>
    {
        public CreateSpotValidator()
        {
            RuleFor(x => x.Coordinates)
                .NotEmpty();

            RuleFor(x => x.Description)
                .NotEmpty();

            RuleFor(x => x.NextCheckup);
                //.GreaterThan(DateTime.UtcNow);
        }
    }
}