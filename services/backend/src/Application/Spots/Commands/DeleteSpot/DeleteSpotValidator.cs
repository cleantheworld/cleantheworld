﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CleanTheWorld.Application.Common.Interfaces.Persistence;
using FluentValidation;

namespace CleanTheWorld.Application.Spots.Commands.DeleteSpot
{
    public class DeleteSpotValidator : AbstractValidator<DeleteSpot>
    {
        private readonly ISpotRepository _repository;
        
        public DeleteSpotValidator(ISpotRepository repository)
        {
            _repository = repository;
            
            RuleFor(x => x.Id)
                .NotEmpty()
                .MustAsync(Exist).WithMessage("Not Found");
        }

        private async Task<bool> Exist(Guid id, CancellationToken cancellationToken) 
            => await _repository.ReadById(id) is not null;
    }
}