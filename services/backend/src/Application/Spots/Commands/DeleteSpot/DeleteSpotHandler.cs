﻿using System.Threading;
using System.Threading.Tasks;
using CleanTheWorld.Application.Common.Interfaces.Persistence;
using MediatR;

namespace CleanTheWorld.Application.Spots.Commands.DeleteSpot
{
    public class DeleteSpotHandler : IRequestHandler<DeleteSpot>
    {
        private readonly ISpotRepository _repository;

        public DeleteSpotHandler(ISpotRepository repository)
        {
            _repository = repository;
        }

        public async Task<Unit> Handle(DeleteSpot request, CancellationToken cancellationToken)
        {
            var spot = await _repository.ReadById(request.Id);
            await _repository.Delete(spot);

            return Unit.Value;
        }
    }
}