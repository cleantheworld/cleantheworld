﻿using System;
using MediatR;

namespace CleanTheWorld.Application.Spots.Commands.DeleteSpot
{
    public class DeleteSpot : IRequest
    {
        public DeleteSpot(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}