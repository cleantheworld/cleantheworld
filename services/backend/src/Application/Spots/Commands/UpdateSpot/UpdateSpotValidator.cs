﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CleanTheWorld.Application.Common.Interfaces.Persistence;
using FluentValidation;

namespace CleanTheWorld.Application.Spots.Commands.UpdateSpot
{
    public class UpdateSpotValidator : AbstractValidator<UpdateSpot>
    {
        private readonly ISpotRepository _repository;
        
        public UpdateSpotValidator(ISpotRepository repository)
        {
            _repository = repository;

            RuleFor(x => x.Id)
                .NotEmpty()
                .MustAsync(Exist).WithMessage("Not Found");
            
            RuleFor(x => x.Description)
                .NotEmpty();

            RuleFor(x => x.NextCheckup);
                //.GreaterThan(DateTime.UtcNow);
        }
        
        private async Task<bool> Exist(Guid id, CancellationToken cancellationToken) 
            => await _repository.ReadById(id) is not null;
    }
}