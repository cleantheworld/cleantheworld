﻿using System.Threading;
using System.Threading.Tasks;
using CleanTheWorld.Application.Common.Interfaces.Persistence;
using CleanTheWorld.Domain.Entities;
using MediatR;

namespace CleanTheWorld.Application.Spots.Commands.UpdateSpot
{
    public class UpdateSpotHandler : IRequestHandler<UpdateSpot, Spot>
    {
        private readonly ISpotRepository _repository;

        public UpdateSpotHandler(ISpotRepository repository)
        {
            _repository = repository;
        }

        public async Task<Spot> Handle(UpdateSpot request, CancellationToken cancellationToken)
        {
            var spot = await _repository.ReadById(request.Id);
            
            spot.UpdateDescription(request.Description);
            spot.UpdatePhotoUrl(request.PhotoUrl);
            spot.SetNextCheckup(request.NextCheckup);

            await _repository.Update(spot);

            return spot;
        }
    }
}