﻿using System;
using CleanTheWorld.Domain.Entities;
using MediatR;

namespace CleanTheWorld.Application.Spots.Commands.UpdateSpot
{
    public class UpdateSpot : IRequest<Spot>
    {
        public UpdateSpot(Guid id, string description, string photoUrl, DateTime nextCheckup)
        {
            Id = id;
            Description = description;
            PhotoUrl = photoUrl;
            NextCheckup = nextCheckup;
        }
        
        public Guid Id { get; set;  }
        public string Description { get; set; }
        public string PhotoUrl { get; set; }
        public DateTime NextCheckup { get; set; }
    }
}