﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CleanTheWorld.Domain.Entities;

namespace CleanTheWorld.Application.Common.Interfaces.Persistence
{
    public interface ISpotRepository
    {
        Task<Spot> ReadById(Guid id);
        Task<IEnumerable<Spot>> ReadAll();
        
        Task Create(Spot entity);
        Task Update(Spot entity);
        Task Delete(Spot entity);
    }
}