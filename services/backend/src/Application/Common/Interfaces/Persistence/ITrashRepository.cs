﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CleanTheWorld.Domain.Entities;

namespace CleanTheWorld.Application.Common.Interfaces.Persistence
{
    public interface ITrashRepository
    {
        Task<Trash> ReadById(Guid id);
        Task<IEnumerable<Trash>> ReadAll();
        
        Task Create(Trash entity);
        Task Update(Trash entity);
        Task Delete(Trash entity);
    }
}