﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CleanTheWorld.Domain.Entities;

namespace CleanTheWorld.Application.Common.Interfaces.Persistence
{
    public interface IParticipantRepository
    {
        Task<Participant> ReadById(Guid id);
        Task<IEnumerable<Participant>> ReadAll();
        
        Task Create(Participant entity);
        Task Update(Participant entity);
        Task Delete(Participant entity);
    }
}