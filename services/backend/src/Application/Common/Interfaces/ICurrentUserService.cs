﻿using System;

namespace CleanTheWorld.Application.Common.Interfaces
{
    public interface ICurrentUserService
    {
        public string GetUserId();
    }
}