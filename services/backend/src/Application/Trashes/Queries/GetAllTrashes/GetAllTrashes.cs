﻿using System.Collections.Generic;
using CleanTheWorld.Domain.Entities;
using MediatR;

namespace CleanTheWorld.Application.Trashes.Queries.GetAllTrashes
{
    public class GetAllTrashes : IRequest<IEnumerable<Trash>>
    {
        
    }
}