﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CleanTheWorld.Application.Common.Interfaces.Persistence;
using CleanTheWorld.Domain.Entities;
using MediatR;

namespace CleanTheWorld.Application.Trashes.Queries.GetAllTrashes
{
    public class GetAllTrashesHandler : IRequestHandler<GetAllTrashes, IEnumerable<Trash>>
    {
        private readonly ITrashRepository _repository;

        public GetAllTrashesHandler(ITrashRepository repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<Trash>> Handle(GetAllTrashes request, CancellationToken cancellationToken)
        {
            return await _repository.ReadAll();
        }
    }
}