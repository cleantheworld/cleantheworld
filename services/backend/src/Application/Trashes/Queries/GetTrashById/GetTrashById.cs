﻿using System;
using CleanTheWorld.Domain.Entities;
using MediatR;

namespace CleanTheWorld.Application.Trashes.Queries.GetTrashById
{
    public class GetTrashById : IRequest<Trash>
    {
        public GetTrashById(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}