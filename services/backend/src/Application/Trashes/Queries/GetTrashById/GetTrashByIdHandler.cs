﻿using System.Threading;
using System.Threading.Tasks;
using CleanTheWorld.Application.Common.Interfaces.Persistence;
using CleanTheWorld.Domain.Entities;
using MediatR;

namespace CleanTheWorld.Application.Trashes.Queries.GetTrashById
{
    public class GetTrashByIdHandler : IRequestHandler<GetTrashById, Trash>
    {
        private readonly ITrashRepository _repository;

        public GetTrashByIdHandler(ITrashRepository repository)
        {
            _repository = repository;
        }

        public async Task<Trash> Handle(GetTrashById request, CancellationToken cancellationToken)
        {
            return await _repository.ReadById(request.Id);
        }
    }
}