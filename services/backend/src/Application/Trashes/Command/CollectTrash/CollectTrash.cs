﻿using System;
using MediatR;

namespace CleanTheWorld.Application.Trashes.Command.CollectTrash
{
    public class CollectTrash : IRequest
    {
        public CollectTrash(Guid trashId)
        {
            TrashId = trashId;
        }
        
        public Guid TrashId { get; set; }
    }
}