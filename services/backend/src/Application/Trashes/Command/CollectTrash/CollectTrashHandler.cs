﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CleanTheWorld.Application.Common.Interfaces;
using CleanTheWorld.Application.Common.Interfaces.Persistence;
using CleanTheWorld.Domain.Entities;
using CleanTheWorld.Domain.Enums;
using MediatR;

namespace CleanTheWorld.Application.Trashes.Command.CollectTrash
{
    public class CollectTrashHandler : IRequestHandler<CollectTrash>
    {
        private readonly ITrashRepository _repository;
        private readonly IParticipantRepository _participants;
        private readonly ICurrentUserService _currentUserService;

            public CollectTrashHandler(ITrashRepository repository, IParticipantRepository participants, ICurrentUserService currentUserService)
            {
                _repository = repository;
                _participants = participants;
                _currentUserService = currentUserService;
            }

        public async Task<Unit> Handle(CollectTrash request, CancellationToken cancellationToken)
        {
            var trash = await _repository.ReadById(request.TrashId);
            trash.SetStatus(TrashStatus.Cleaned);

            var participant = await _participants.ReadAll();
            var participants = participant as Participant[] ?? participant.ToArray();
            
            if (participants.Any(x => x.Auth0Id == _currentUserService.GetUserId()))
            {
                var thisParticipant = participants.First(x => x.Auth0Id == _currentUserService.GetUserId());
                thisParticipant.AddPoints(10);
                trash.SetAssociatedParticipant(thisParticipant);
            }
            else
            {
                var newParticipant = new Participant("App User", _currentUserService.GetUserId());
                newParticipant.AddPoints(10);
                await _participants.Create(newParticipant);
                trash.SetAssociatedParticipant(newParticipant);
            }
            
            await _repository.Update(trash);
            
            return Unit.Value;
        }
    }
}