﻿using System;
using CleanTheWorld.Domain.Entities;
using MediatR;

namespace CleanTheWorld.Application.Trashes.Command.UpdateTrash
{
    public class UpdateTrash : IRequest<Trash>
    {
        public UpdateTrash(Guid id, string description, string photoUrl)
        {
            Id = id;
            Description = description;
            PhotoUrl = photoUrl;
        }

        public Guid Id { get; set; }
        public string Description { get; set; }
        public string PhotoUrl { get; set; }
    }
}