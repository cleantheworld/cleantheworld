﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CleanTheWorld.Application.Common.Interfaces.Persistence;
using FluentValidation;

namespace CleanTheWorld.Application.Trashes.Command.UpdateTrash
{
    public class UpdateTrashValidator : AbstractValidator<UpdateTrash>
    {
        private readonly ITrashRepository _repository;
        
        public UpdateTrashValidator(ITrashRepository repository)
        {
            _repository = repository;
            
            RuleFor(x => x.Id)
                .NotEmpty()
                .MustAsync(Exist).WithMessage("Not Found");

            RuleFor(x => x.Description)
                .NotEmpty();
        }
        
        private async Task<bool> Exist(Guid id, CancellationToken cancellationToken)
            => await _repository.ReadById(id) is not null;
    }
}