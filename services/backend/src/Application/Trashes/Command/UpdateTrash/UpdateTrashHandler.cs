﻿using System.Threading;
using System.Threading.Tasks;
using CleanTheWorld.Application.Common.Interfaces.Persistence;
using CleanTheWorld.Domain.Entities;
using MediatR;

namespace CleanTheWorld.Application.Trashes.Command.UpdateTrash
{
    public class UpdateTrashHandler : IRequestHandler<UpdateTrash, Trash>
    {
        private readonly ITrashRepository _repository;

        public UpdateTrashHandler(ITrashRepository repository)
        {
            _repository = repository;
        }

        public async Task<Trash> Handle(UpdateTrash request, CancellationToken cancellationToken)
        {
            var trash = await _repository.ReadById(request.Id);
            
            trash.UpdateDescription(request.Description);
            trash.UpdatePhotoUrl(request.PhotoUrl);
            
            await _repository.Update(trash);
            
            return trash;
        }
    }
}