﻿using System;
using MediatR;

namespace CleanTheWorld.Application.Trashes.Command.ReserveTrash
{
    public class ReserveTrash : IRequest
    {
        public ReserveTrash(Guid trashId)
        {
            TrashId = trashId;
        }

        public Guid TrashId { get; set; }
    }
}