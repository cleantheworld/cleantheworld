﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CleanTheWorld.Application.Common.Interfaces;
using CleanTheWorld.Application.Common.Interfaces.Persistence;
using CleanTheWorld.Domain.Entities;
using CleanTheWorld.Domain.Enums;
using MediatR;

namespace CleanTheWorld.Application.Trashes.Command.ReserveTrash
{
    public class ReserveTrashHandler : IRequestHandler<ReserveTrash>
    {
        private readonly ITrashRepository _repository;
        private readonly IParticipantRepository _participants;
        private readonly ICurrentUserService _currentUserService;

        public ReserveTrashHandler(ITrashRepository repository, IParticipantRepository participants, ICurrentUserService currentUserService)
        {
            _repository = repository;
            _participants = participants;
            _currentUserService = currentUserService;
        }

        public async Task<Unit> Handle(ReserveTrash request, CancellationToken cancellationToken)
        {
            var trash = await _repository.ReadById(request.TrashId);
            
            trash.SetStatus(TrashStatus.Taken);
            
            var participant = await _participants.ReadAll();
            var participants = participant as Participant[] ?? participant.ToArray();
            
            if (participants.Any(x => x.Auth0Id == _currentUserService.GetUserId()))
            {
                trash.SetAssociatedParticipant(participants.First(x => x.Auth0Id == _currentUserService.GetUserId()));
            }
            else
            {
                var newParticipant = new Participant("App User", _currentUserService.GetUserId());
                await _participants.Create(newParticipant);
                trash.SetAssociatedParticipant(newParticipant);
            }
            
            await _repository.Update(trash);
            
            return Unit.Value;
        }
    }
}