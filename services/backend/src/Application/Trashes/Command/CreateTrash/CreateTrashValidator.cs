﻿using FluentValidation;

namespace CleanTheWorld.Application.Trashes.Command.CreateTrash
{
    public class CreateTrashValidator : AbstractValidator<CreateTrash>
    {
        public CreateTrashValidator()
        {
            RuleFor(x => x.Coordinates)
                .NotEmpty();

            RuleFor(x => x.Description)
                .NotEmpty();
        }
    }
}