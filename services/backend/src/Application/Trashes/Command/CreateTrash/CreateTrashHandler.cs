﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CleanTheWorld.Application.Common.Interfaces.Persistence;
using CleanTheWorld.Domain.Entities;
using MediatR;

namespace CleanTheWorld.Application.Trashes.Command.CreateTrash
{
    public class CreateTrashHandler : IRequestHandler<CreateTrash, Trash>
    {
        private readonly ITrashRepository _repository;

        public CreateTrashHandler(ITrashRepository repository)
        {
            _repository = repository;
        }

        public async Task<Trash> Handle(CreateTrash request, CancellationToken cancellationToken)
        {
            var trash = new Trash(request.Coordinates, request.Description, request.PhotoUrl);
            
            await _repository.Create(trash);
            
            return trash;
        }
    }
}