﻿using CleanTheWorld.Domain.Entities;
using MediatR;

namespace CleanTheWorld.Application.Trashes.Command.CreateTrash
{
    public class CreateTrash : IRequest<Trash>
    {
        public CreateTrash(Coordinates coordinates, string description, string photoUrl)
        {
            Coordinates = coordinates;
            Description = description;
            PhotoUrl = photoUrl;
        }

        public Coordinates Coordinates { get; set; }
        public string Description { get; set; }
        public string PhotoUrl { get; set; }
    }
}