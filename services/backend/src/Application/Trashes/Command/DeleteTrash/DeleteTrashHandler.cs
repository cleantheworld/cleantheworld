﻿using System.Threading;
using System.Threading.Tasks;
using CleanTheWorld.Application.Common.Interfaces.Persistence;
using MediatR;

namespace CleanTheWorld.Application.Trashes.Command.DeleteTrash
{
    public class DeleteTrashHandler : IRequestHandler<DeleteTrash>
    {
        private readonly ITrashRepository _repository;

        public DeleteTrashHandler(ITrashRepository repository)
        {
            _repository = repository;
        }

        public async Task<Unit> Handle(DeleteTrash request, CancellationToken cancellationToken)
        {
            var trash = await _repository.ReadById(request.Id);
            await _repository.Delete(trash);
            
            return Unit.Value;
        }
    }
}