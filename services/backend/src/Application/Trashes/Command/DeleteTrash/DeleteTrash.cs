﻿using System;
using MediatR;

namespace CleanTheWorld.Application.Trashes.Command.DeleteTrash
{
    public class DeleteTrash : IRequest
    {
        public DeleteTrash(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}