﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CleanTheWorld.Application.Common.Interfaces.Persistence;
using FluentValidation;

namespace CleanTheWorld.Application.Trashes.Command.DeleteTrash
{
    public class DeleteTrashValidator : AbstractValidator<DeleteTrash>
    {
        private readonly ITrashRepository _repository;
        
        public DeleteTrashValidator(ITrashRepository repository)
        {
            _repository = repository;

            RuleFor(x => x.Id)
                .NotEmpty()
                .MustAsync(Exist).WithMessage("Not Found");
        }

        private async Task<bool> Exist(Guid id, CancellationToken cancellationToken)
            => await _repository.ReadById(id) is not null;
    }
}