﻿using System;
using CleanTheWorld.Domain.Entities;

namespace CleanTheWorld.UnitTests.Application.Spots.Mocks
{
    public class MockedSpot : Spot
    {
        public MockedSpot(Guid id, DateTime created, DateTime updated, Coordinates coordinates, string description, string photoUrl, DateTime nextCheckup) : base(id, created, updated, coordinates, description, photoUrl, nextCheckup)
        {
            
        }
    }
}