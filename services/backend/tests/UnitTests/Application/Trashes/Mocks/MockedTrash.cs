﻿using System;
using CleanTheWorld.Domain.Entities;

namespace CleanTheWorld.UnitTests.Application.Trashes.Mocks
{
    public class MockedTrash : Trash
    {
        public MockedTrash(Guid id, DateTime created, DateTime updated, Coordinates coordinates, string description, string photoUrl) : base(id, created, updated, coordinates, description, photoUrl)
        {
            
        }
    }
}