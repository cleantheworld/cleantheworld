# CleanTheWorld - Backend

## Maintainers

* [Stanisław Nieradko](https://nieradko.com)
* [Błażej Sak](https://github.com/bsak2003)

# Jak uruchomić aplikację?

## Auth0

Aplikacja wykorzystuje Auth0 do autentykacji. Aby umożliwić aplikacji działanie, należy:
- zarejestrować się w [Auth0](https://manage.auth0.com/) i utworzyć nowy projekt
- utworzyć nowe API
  ![Auth0 Create API](img/new-api.png)
- skopiować wartości Audience i Authority, który znajdują się w zakładce 'Quick Start' w nowo utworzonym API
  ![Auth0 Quickstart](img/quickstart.png)
- przejdź do zakładki Test, kliknij na link `You can inspect how this token is built at jwt.io`
  ![Auth0 Test](img/test.png)
- w jwt.io skopiuj Public Key i przekonwertuj go na format XML [tutaj](https://superdry.apphb.com/tools/online-rsa-key-converter)
  ![Auth0 JwtIo](img/jwtio.png)
- klucz w formacie xml zapisz jako rsa.xml (ten plik będzie potrzebny później)
  ![Auth0 Pem2Xml](img/pem2xml.png)

## Kompilacja z kodu źródłowego

- Pobierz kod źródłowy aplikacji z GitLaba (git clone https://gitlab.com/cleantheworld/cleantheworld.git) lub w formie pliku [zip](https://gitlab.com/cleantheworld/cleantheworld/-/archive/main/cleantheworld-main.zip)
- skopiuj plik `rsa.xml` do lokalizacji `src/Api`
- dostosuj wartości Audience i Authority w pliku Auth0Utils.cs (`./src/Api/Utils`)
- z poziomu folderu repozytorium uruchom komendę `./build.cmd dbupdate`
- z poziomu folderu `./src/Api` uruchom komendę `dotnet run`