# CleanTheWorld - Frontend

## Maintainers

* [Piotr Trybisz](https://ptrybisz.tk)

## Instrukcja instalacji

- dostsouj wartość API_URL w pliku src/main.ts i Domain/client_id/Audience w src/App.svelte (zgodnie z backendem)
- zainstaluj [NodeJS](https://nodejs.org/en/)
- uruchom z folderu aplikacji `npm install` by zainstalować zależności
- uruchom aplikacje przez 'npm run dev'