import App from './App.svelte';
const app = new App({
	target: document.getElementById("app"),
	props: {"api_url":"https://cleantheworld.bazik.xyz/api"}
});